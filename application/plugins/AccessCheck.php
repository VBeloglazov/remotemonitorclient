<?php

class Application_Plugin_AccessCheck extends Zend_Controller_Plugin_Abstract {

    private $_acl = null;
    private $_auth = null;

    public function __construct(Zend_Acl $acl, Zend_Auth $auth) {
        $this->_acl = $acl;
        $this->_auth = $auth;
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        $identity = $this->_auth->getStorage()->read();
        $role = !empty($identity->uid) ? 'user' : 'guest';
        if ($controller == 'cron') {
            //trace($this->getRequest()->getParams(),1);
            if (in_array($this->getRequest()->getParam('akey'), array('ejjLT8s9'))) {
                $role = 'cron';
            }
        }
        Zend_Registry::set('sessuser', $identity);
        if (!$this->_acl->isAllowed($role, $controller, $action)) {
            if ($role != 'guest') {
                $request
                        ->setControllerName('error')
                        ->setActionName('error');
            } else {

                $request->setControllerName('index')->setActionName('login');
            }
        } elseif ($role == 'user') {
            $_dbUser = new Application_Model_DbTable_User();
            $_dbUser->setOnline($identity->uid);
        }
        //trace($request,1);
    }

}
