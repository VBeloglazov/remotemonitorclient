<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initConfig() {
        $opts = $this->getOptions();
        $config = new Zend_Config($opts, true);

        Zend_Registry::set('config', $config);
        return $config;
    }

    
    protected  function _initRegistry()
    {
        // get options overrided in DB
        $this->bootstrap('db');

//        $dbOptions = new Application_Model_DbTable_SystemSettings();
//        $aOptions = $dbOptions->getOptionsAsArray();
//        if(is_array($aOptions) && !empty($aOptions)){
//            $this->setOptions($aOptions);
//        }

        // reinit config
        $this->_initConfig();

        //trace(Zend_Registry::get('config')->toArray());
    }
    


    protected function _initAutoload() {
        Zend_Loader_Autoloader::getInstance()->registerNamespace(
                array(
                    'Inlisu_'
                )
        );
        require 'Zend/Loader/AutoloaderFactory.php';
        Zend_Loader_AutoloaderFactory::factory(
            array (
                
            ));
    }

    protected function _initLogger() {
        $options = $this->getOptions();
        $resource = $this->getPluginResource('db');
        $dbAdapter = $resource->getDbAdapter();
        $logger = Zend_Log::factory(array(
                    //'timestampFormat' => 'U',
                    array(
                        'writerName' => 'Db',
                        'writerParams' => array(
                            'db' => $dbAdapter,
                            'table' => $options['db']['prefix'] . 'log',
                            'columnMap' => array(
                                'uid' => 'uid',
                                'priority' => 'priority',
                                'trace' => 'trace',
                                'mssg' => 'message',
                                'dt' => 'timestamp',
                                'userinfo' => 'userinfo',
                                'system' => 'system',
                                'subsystem' => 'subsystem',
                                'status' => 'status',
                            )
                        ),
                        'filterName' => 'Priority',
                        'filterParams' => array(
                            'priority' => Zend_Log::INFO,
                        ),
                    ),
                    //TODO: add mail to admin for errors
                    array(
                        'writerName' => 'Firebug',
                        'filterName' => 'Priority',
                        'filterParams' => array(
                            'priority' => Zend_Log::DEBUG,
                        ),
                    ),
        ));
        $logger->setEventItem('userinfo', $_SERVER['REMOTE_ADDR']." ".$_SERVER['HTTP_USER_AGENT']);
        if (isset(Zend_Auth::getInstance()->getStorage()->read()->uid)) $logger->setEventItem('uid', Zend_Auth::getInstance()->getStorage()->read()->uid);
        Zend_Registry::set('Zend_Log', $logger);
    }

    protected function _initAccess() {
        $acl = new Zend_Acl();
        $acl->addResource('index');
        $acl->addResource('monitor');
        $acl->addResource('cron');
        $acl->addResource('test');
        $acl->addResource('error');


        $acl->addRole('guest');
        $acl->addRole('admin');
        $acl->addRole('cron');
        $acl->addRole('user', 'guest');


        $acl->allow('guest', 'index', array('index',
            'login',
            'logout')
        );


        $acl->allow('user', 'monitor', array('index','getdata','log','changedata'));
        $acl->allow('user', 'test', array('index'));
        $acl->allow('user', 'index', array('apitest'));
        $acl->allow('cron', 'cron', array('update-pool-data'));


        $fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new Application_Plugin_AccessCheck($acl, Zend_Auth::getInstance()));

        $namespace = new Zend_Session_Namespace('myHash');
        if (!isset($namespace->hash))
            $namespace->hash = md5(time() . 'd8823e3156348f5bae6dacd436c919c6');
    }
    
protected function _initZFDebug()
    {
        if (getenv('APPLICATION_ENV') != 'development') return false;
        $options = $this->getOptions();
        if (empty($options['zfdebug']['enabled'])) return false;
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('ZFDebug');

        $options = array(
            'plugins' => array(
                'Variables',
                'File' => array('base_path' => APPLICATION_PATH),
                'Memory',
                'Time',
                //'Registry',
                'Exception',
                'Html',
                'Database')
        );

        # Instantiate the database adapter and setup the plugin.
        # Alternatively just add the plugin like above and rely on the autodiscovery feature.
        if ($this->hasPluginResource('db')) {
            $this->bootstrap('db');
            /** @var Zend_Application_Resource_Db $resource */
            $resource = $this->getPluginResource('db');
            $db = $resource->getDbAdapter();
            $options['plugins']['Database']['adapter'] = $db;
        }

        # Setup the cache plugin
        if ($this->hasPluginResource('cache')) {
            $this->bootstrap('cache');
            $cache = $this->getPluginResource('cache')->getDbAdapter();
            $options['plugins']['Cache']['backend'] = $cache->getBackend();
        }

        $debug = new ZFDebug_Controller_Plugin_Debug($options);

        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');
        $frontController->registerPlugin($debug);
        return true;
    }

}
