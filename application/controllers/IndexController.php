<?php

class IndexController extends Inlisu_Controller_Abstract {

    public function init() {
        $front = Zend_Controller_Front::getInstance();
        $bootstrap = $front->getParam('bootstrap');
        $options = $bootstrap->getOptions();
        $this->view->site_shortname = $options['site']['shortname'];
        $this->view->site_name = $options['site']['name'];
        $this->view->oSession = Zend_Auth::getInstance()->getStorage()->read();
        $this->Logger()->setEventItem('system', 'main');
        $this->Logger()->setEventItem('subsystem', $this->getRequest()->getActionName());
        //$this->view->headScript()->appendFile('/js/public.js');
        $this->view->def_notifications = array();
    }

    public function indexAction() {
        $this->_forward('login');
    }

    public function loginAction() {
        if ($this->isAjax() && $this->getRequest()->isPost()) {
            exit('<script>window.location.replace("/index/login")</script>');
            return;
        }
        $this->_helper->layout->setlayout("minimal");
        $this->view->form = $form = new Application_Form_Login();
        if (!empty($this->view->oSession->uid))
            $this->_redirect('/monitor/');
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if (!$form->isValid($formData)) {
                $this->view->form_errors = $form->getMessages();
            } else {
                $_mPapi = new Application_Model_Papi($formData['login'],$formData['password']);
                if($_mPapi->getSessionId()){
                    //trace($_mPapi,1);
                    $dbUser = new Application_Model_DbTable_User();
                    $userdata = (object)['sid'=>$_mPapi->getSessionId(),'poolList'=>$_mPapi->getPoolList(),'login'=>$formData['login']];
                    if($user = $dbUser->getUserbyLogin($formData['login'])){
                        $userdata->uid=$user->id;
                    } else {
                        $userdata->uid=$dbUser->insert(array('name'=>  addslashes($formData['login'])));
                    }
                    $dbUser->setOnline($userdata->uid);
                    $dbUser->setSid($userdata->uid,$_mPapi->getSessionId());
                    Zend_Auth::getInstance()->getStorage()->write($userdata);
                    $this->Logger()->setEventItem('uid', $user->id);
                    $this->Logger()->setEventItem('status', 'OK');
                    $this->Logger()->INFO('Successful Authorization. sessionId: '.$userdata->sid);
                    $this->_redirect($_SERVER['HTTP_REFERER']);
                    
                } else {
                    $this->view->def_notifications[] = array('type' => "error", "title" => "Authorization Error", "message" => "Wrong login or password");
                    $this->Logger()->setEventItem('status', 'FAIL');
                    $this->Logger()->NOTICE('Failed login "' . $formData['login'].'"');
                }

            }
        }
    }


    public function logoutAction() {
        $_mPapi = new Application_Model_Papi();
        $_mPapi->logout();
        Zend_Auth::getInstance()->clearIdentity();
        $this->Logger()->setEventItem('status', 'OK');
        $this->Logger()->INFO('sessionId: '.$this->view->oSession->sid);
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->getHelper('layout')->disableLayout();
        $this->_redirect('/');
    }
    public function apitestAction() {
        $this->_helper->viewRenderer->setNoRender();
//        $_mPapi = new Application_Model_Papi();
//        $_mPapi->DebugOn();
//        echo '<br>********* Debug On *********';
//        echo '<br>********* updatePoolList() *********';
//        $_mPapi->updatePoolList();
//        echo '<br>********* getPoolData all pools *********';
//        $_mPapi->getPoolData();
//        echo '<br>********* getPoolData only pool with id 3 *********';
//        $_mPapi->getPoolData(0,array(3));
//        echo '<br>********* getPoolHistory(2) pool with id 2 *********';
//        $_mPapi->getPoolHistory('2');
        $mode = 'compact';
        $mPools = new Application_Model_Monitor($mode);
        $mPools->updatePools();
        
        
    }

}
