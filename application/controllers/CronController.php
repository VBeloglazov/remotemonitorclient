<?php

class CronController extends Inlisu_Controller_Abstract {

    public function init() {
        $this->Logger()->setEventItem('system', 'cron');
        $this->Logger()->setEventItem('subsystem', $this->getRequest()->getActionName());
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    public function indexAction() {

        try {

        } catch (Exception $e) {


            echo $e->getMessage();
        }
    }
    public function updatePoolDataAction() {
        $dbParams = new Application_Model_DbTable_Params();
        $lastFinish = $dbParams->getDataByParamName('cron_updatePoolData_lastFinish')->val;
        $lock = $dbParams->getDataByParamName('cron_updatePoolData_lock')->val;
        $timeout = $dbParams->getDataByParamName('cron_updatePoolData_timeout')->val;
        if( $lastFinish>(time()-$timeout) or $lock) exit ();
        
        $dbParams->storeData('cron_updatePoolData_lock', 1);
        $mPools = new Application_Model_Monitor();
        $mPools->updatePools();
        $dbParams->storeData('cron_updatePoolData_lastFinish', time());
        $dbParams->storeData('cron_updatePoolData_lock', 0);
    }

}
