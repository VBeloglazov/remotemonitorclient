<?php

class MonitorController extends Inlisu_Controller_Abstract {

    public function init() {
        $front = Zend_Controller_Front::getInstance();
        $bootstrap = $front->getParam('bootstrap');
        $options = $bootstrap->getOptions();
        $this->view->site_shortname = $options['site']['shortname'];
        $this->view->site_name = $options['site']['name'];
        $this->view->oSession = Zend_Auth::getInstance()->getStorage()->read();
        $this->_dbUser = new Application_Model_DbTable_User();
        $this->Logger()->setEventItem('system', 'monitor');
        $this->Logger()->setEventItem('subsystem', $this->getRequest()->getActionName());
        $this->view->headScript()->appendFile('/js/monitor.js');
        $this->view->def_notifications = array();

    }

    public function indexAction() {
        $this->view->headScript()->appendFile('/js/plugins/contextMenu/jquery.contextMenu.min.js')
                ->appendFile('/js/plugins/contextMenu/jquery.ui.position.min.js');
        $this->view->headLink()->appendStylesheet('/css/plugins/contextMenu/jquery.contextMenu.min.css');
        $mPools = new Application_Model_Monitor('compact');
        
        $user =  $this->_dbUser->getUserbyId($this->view->oSession->uid);
        

        
        $this->view->allGroupsList = $mPools->getAllGroupsList();
        
    }
    
    public function getdataAction() {
        $formData = $this->getRequest()->getPost();
        $mode = (isset($formData['mode']) and $formData['mode']=='detail')?'detail':'compact';
        $mPools = new Application_Model_Monitor($mode);
        
        if(isset($formData['sortofilter']) and is_array($formData['sortofilter'])){
            $mPools->saveSortofilter($formData['sortofilter']);
        }
        exit($mPools->getAllPoolsJson());
        
    }
    
    public function logAction() {
        $filter = array();
        $order = '';
        $PLogs = new Application_Model_Plogs();
        $formData = $this->getRequest()->getPost();
        $page = isset($formData['page'])&&(int)$formData['page']?(int)$formData['page']:1;

        $column_list = array('uname','dt','pname','col','vprev','vset','error');
        If (isset($formData['order'])&&in_array($formData['order'], array('asc','desc'))&&
                isset($formData['order_column'])&&in_array($formData['order_column'], $column_list)
                ){
            $this->view->order = array('column'=>$formData['order_column'],'order'=>$formData['order']);
            $order = $formData['order_column'].' '.$formData['order'];
        }
        If (isset($formData['date_from'])&&!empty($formData['date_from'])){
            $filter['date_from']=  strtotime($formData['date_from']);
        }
        If (isset($formData['date_to'])&&!empty($formData['date_to'])){
            $filter['date_to']=  strtotime($formData['date_to']);
        }
        If (isset($formData['filter_column'])&&in_array($formData['filter_column'], $column_list)&&
                isset($formData['filter_text'])&&!empty($formData['filter_text'])
                ){
            $filter['column']= $formData['filter_column'];
            $filter['text']= $formData['filter_text'];
        }
        
        $data = $PLogs->getLogs($filter,$order);
        $adapter = new Zend_Paginator_Adapter_DbSelect($data);
        if (isset($formData['csv']) and $formData['csv']) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            header( 'Content-Type: text/csv' );
            //header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename="log.csv"');
            $fp = fopen('php://output', 'w');
            foreach ($adapter->getItems(0,0) as $value) {
                fputcsv($fp, array($value['uname'],$value['dt'],$value['pname'],$value['col'],$value['vprev'],$value['vset'],$value['error']));
            }
            fclose($fp);
            return;
        }
        $paginator = new Zend_Paginator($adapter); // setup Pagination
        $paginator->setItemCountPerPage(50);
        $paginator->setCurrentPageNumber($page); // current page
        $this->view->paginator = $paginator;
        $this->view->filter = $filter;
        Zend_Paginator::setDefaultScrollingStyle('Sliding');
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('partials/paginator.phtml');
        
        //$this->view->logs =  $PLogs->getLogs();
    }
    public function changedataAction() {
        $canChangeValue=1;$satus=0;
        $formData = $this->getRequest()->getPost();
        $itm_list = array('pool_chlor_out','spa_chlor_out','pool_point','spa_point');
        if (!isset($formData['itm']) or !in_array($formData['itm'], $itm_list) or (int)$formData['pid']==0) exit(Zend_Json::encode(array('status' => 'error')));
        $PLogs = new Application_Model_Plogs();
        if(in_array($formData['itm'], array('pool_point','spa_point'))){
            if (!isset($formData['hmode']) or empty($formData['hmode'])) exit(Zend_Json::encode(array('status' => 'error')));
            $satus = $PLogs->addLog($formData['pid'],$formData['itm']=='pool_point'?'pool_hmode':'spa_hmode',$formData['hmode'],$this->view->oSession->uid);
            if($formData['hmode']=='Off') $canChangeValue = 0;
        }
        
        if($canChangeValue) $satus = $PLogs->addLog($formData['pid'],$formData['itm'],$formData['val'],$this->view->oSession->uid);
        if ($satus){
            exit(Zend_Json::encode(array('status' => 'ok')));
        } else
            exit(Zend_Json::encode(array('status' => 'error')));
        
    }

}
