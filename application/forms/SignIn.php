<?php

class Application_Form_SignIn extends Zend_Form
{

    public function init()
    {
            $front = Zend_Controller_Front::getInstance();
            $bootstrap = $front->getParam('bootstrap');
            $options = $bootstrap->getOptions();
   
            
            $this->setName('signinform');
            
            $elm = new Zend_Form_Element_Text('login',array( 'size' => 30,'minlength'=>3, 'maxlength' => 50));
            $elm->setLabel("Логин")
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty', true,
                        array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                ->addValidator('Db_NoRecordExists', true, array(
                    'table'     => $options['db']['prefix'].'users',
                    'field'     => 'login',
                    'messages' => array(
                       Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => 'Этот Логин уже занят')
                 )) ;     
            $this->addElement($elm);
            
            $elm = new Zend_Form_Element_Text('lname');
            $elm->setLabel("Фамилия")
                ->setRequired(true)
                ->addValidator('NotEmpty', true,array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                 ->addFilter('StripTags')
                ->addFilter('StringTrim');   
            $this->addElement($elm);
            
            $elm = new Zend_Form_Element_Text('fname');
            $elm->setLabel("Имя")
                ->setRequired(true)
                ->addValidator('NotEmpty', true,array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                 ->addFilter('StripTags')
                ->addFilter('StringTrim');   
            $this->addElement($elm);
            
            $elm = new Zend_Form_Element_Text('mname');
            $elm->setLabel("Отчество")
//                ->setRequired(true)
//                ->addValidator('NotEmpty', true,array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                 ->addFilter('StripTags')
                ->addFilter('StringTrim');   
            $this->addElement($elm);
            

            
            $elm = new Zend_Form_Element_Text('email',array( 'size' => 30,'minlength'=>6, 'maxlength' => 128));
            $elm->setLabel("E-mail адрес")
                ->setRequired(true)
                ->addFilter('StripTags')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty', true,
                        array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                ->addValidator('EmailAddress',true,array(
                   'messages' => array(
                       Zend_Validate_EmailAddress::INVALID            => "Email is incorrect",
                       Zend_Validate_EmailAddress::INVALID_FORMAT     => "Email is incorrect",
                       Zend_Validate_EmailAddress::INVALID_HOSTNAME   => "Email is incorrect",
                       Zend_Validate_EmailAddress::INVALID_MX_RECORD  => "Email is incorrect",
                       Zend_Validate_EmailAddress::INVALID_SEGMENT    => "Email is incorrect",
                       Zend_Validate_EmailAddress::DOT_ATOM           => "Email is incorrect",
                       Zend_Validate_EmailAddress::QUOTED_STRING      => "Email is incorrect",
                       Zend_Validate_EmailAddress::INVALID_LOCAL_PART => "Email is incorrect",
                       Zend_Validate_EmailAddress::LENGTH_EXCEEDED    => "Email is incorrect",
                    ) 
                    
                    
                )) 
                ->addValidator('Db_NoRecordExists', true, array(
                    'table'     => $options['db']['prefix'].'users',
                    'field'     => 'email',
                    'messages' => array(
                       Zend_Validate_Db_NoRecordExists::ERROR_RECORD_FOUND => 'Этот E-mail уже занят')
                 )) ;     
            $this->addElement($elm);

            
            
            $elm = new Zend_Form_Element_Password('pass');
            $elm->setLabel('Пароль')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                ->addValidator('stringLength', true, array("min"=>5,"max"=>20));       
            $this->addElement($elm);
            $elm = new Zend_Form_Element_Password('cpass');
            $elm->setLabel('Пароль повторно')
                ->setRequired(true)
                ->addValidator('NotEmpty', true, array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                ->addPrefixPath('Valid', APPLICATION_LIBRARY.'/Valid', 'validate')     
                ->addValidator('PasswordConfirmation',true);   
            $this->addElement($elm); 
            
//            $elm = new Zend_Form_Element_Text('phone');
//            $elm->setLabel('Мобильный телефон')
//                ->setRequired(true)
//                ->addFilter('StripTags')
//                ->addFilter('StringTrim')
//                ->addValidator('NotEmpty', true, array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
//                ->addPrefixPath('Valid', APPLICATION_LIBRARY.'/Valid', 'validate')
//                ->addValidator('PhoneNumberCheck',true);
//            $this->addElement($elm);
            
            $elm = new Zend_Form_Element_Text('agency_title');
            $elm->setLabel("Агентство")
//                ->setRequired(true)
//                ->addValidator('NotEmpty', true,array( 'messages' => array(Zend_Validate_NotEmpty::IS_EMPTY => "Значение не может быть пустым" )))
                 ->addFilter('StripTags')
                ->addFilter('StringTrim');   
            $this->addElement($elm);
            
            
            $elm = new Zend_Form_Element_Submit('signup');
            $this->addElement($elm);
            $this->setMethod('post');
    }


}

