<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setName('login');
        $elm = new Zend_Form_Element_Text('login');
        $elm->setRequired(true)
            ->setAttrib("class", "input")
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true, array('messages' => array('isEmpty' =>  'Введите Логин')));
        $this->addElement($elm);
        $elm = new Zend_Form_Element_Password('password');
        $elm->setRequired(true)
            ->setAttrib("class", "input")
            ->addValidator('NotEmpty', true,  array('messages' => array('isEmpty' => 'Введите Пароль')));
        $this->addElement($elm);
        $elm = new Zend_Form_Element_Submit('submit');
        $elm->setAttrib("class", "submit");
        $elm->setLabel('Send');
        $this->addElement($elm);
        $this->setMethod('post');
    }


}

