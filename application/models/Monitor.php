<?php

class Application_Model_Monitor extends Application_Model_Abstract {

    protected $id, $name, $pools = FALSE, $mode = 'compact',
            $_dbPools, $_oSession;

    public function __construct($mode = 'compact') {
        $auth = Zend_Auth::getInstance();
        $this->_oSession = $auth->getStorage()->read();
        $this->_dbPools = new Application_Model_DbTable_Pools();
        $this->setMode($mode);
    }

    //type = 'string', 'number', 'boolean', 'date', 'datetime', and 'timeofday'.

    public function getHeader() {

        $allgroups['compact'] = array("groups" => array(
                array('name' => 'installation', 'htmlRowVal' => '<div class="tbl_cell  flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell wb320"><div class="tbl_wrapper group"><div class="tbl_row" ></div></div></div>',
                    'cols' => array(
                        array('name' => 'status', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb40"><div class="res_wrp"><div  id="itm-status-class" class="ico_status"></div></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb40"><div class="header_cell box_elem onlystatus"><div class="cw"><div class="after-img-light">Status</div></div></div></div>'),
                        array('name' => 'name', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell  wb180"><div id="itm-name-val" class="res_wrp nowrap text-left"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb180"><div class="header_cell box_elem"><div class="cw"><span>Installation Name</span></div></div></div>'),
                        array('name' => 'air_t', 'has_sort' => 0, 'valSuffix' => '°F', 'htmlRowVal' => '<div class="tbl_cell wb100"><div id="itm-air_t-val" class="res_wrp"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb100"><div class="header_cell box_elem icon1_bgr"><div class="cw"><span>Air<br>Temp.</span></div></div></div>'),
                    )),
                array('name' => 'pool', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row wb160"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group"><div class="tbl_row wb160"></div></div></div>',
                    'cols' => array(
                        array('name' => 'pool_pump', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb60"><div id="itm-pool_pump-val" class="res_wrp txt_status"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb60"><div class="header_cell box_elem"><i class="icn icon2 one"></i><div class="after-img-light">Pool</div></div></div>'),
                        array('name' => 'pool_t', 'has_sort' => 0, 'valSuffix' => '°F', 'htmlRowVal' => '<div class="tbl_cell wb140"><div id="itm-pool_t-val" class="res_wrp temp lv mw90"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb140"><div class="header_cell box_elem"><div class="cw"><span>Pool<br>Temp</span></div></div></div>'),
                    )),
                array('name' => 'spa', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row wb160"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group"><div class="tbl_row wb160"></div></div></div>',
                    'cols' => array(
                        array('name' => 'spa_pump', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb60"><div  id="itm-spa_pump-val" class="res_wrp txt_status off">OFF</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb60"><div class="header_cell box_elem"><i class="icn icon3 one"></i><div class="after-img-light" style="text-align: center;">Spa</div></div></div>'),
                        array('name' => 'spa_t', 'has_sort' => 0, 'valSuffix' => '°F', 'htmlRowVal' => '<div class="tbl_cell wb140"><div  id="itm-spa_t-val" class="res_wrp spa-degree temp lv mw90 nowrap"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb140"><div class="header_cell box_elem"><div class="cw"><span>Spa<br>Temp</span></div></div></div>'),
                    )),
                array('name' => 'lights', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row wb200"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell pluspx"><div class="tbl_wrapper group"><div class="tbl_row wb200"></div></div></div>',
                    'cols' => array(
                        array('name' => 'lights', 'has_sort' => 0, 'htmlRowVal' => '<div class="tbl_cell wb60"><div class="res_wrp wb60 from_to nowrap hasv "><span  id="itm-lights_on-val" class="from">7</span><span class="delim"> / </span><span  id="itm-lights_total-val" class="to">20</span></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb60"><div class="header_cell box_elem"><i class="icn icon4 one"></i><div class="after-img-light">Lights</div></div></div>'),
                        array('name' => 'spa_remote', 'has_sort' => 0, 'htmlRowVal' => '<div class="tbl_cell wb70"><div id="itm-spa_remote-val" class="res_wrp wb70">Enabled</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb70"><div class="header_cell box_elem  wb70"><div class="cw"><span>Remote</span></div></div></div>'),
                        array('name' => 'intelliflow', 'has_sort' => 0, 'htmlRowVal' => '<div class="tbl_cell wb70"><div id="itm-intelliflow-val" class="res_wrp wb70">Yes</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb70"><div class="header_cell box_elem wb70"><div class="cw"><span>IntelliFlo</span></div></div></div>'),
                    )),
                array('name' => 'alarms', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell pluspx"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'alarms', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb80"><div id="itm-alarms-val" class="res_wrp txt_label danger">3</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell wb80 noact "><div class="header_cell"><div class="cw"><span>Alarms</span></div></div></div>'),
                    )),
                array('name' => 'ph', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell pluspx"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'ph', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb50"><div id="itm-ph-val" class="res_wrp txt_label success">7.51</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell wb50 noact"><div class="header_cell"><div class="cw"><span>pH</span></div></div></div>'),
                    )),
                array('name' => 'orp', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell pluspx"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'orp', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb60"><div id="itm-orp-val" class="res_wrp txt_label warning btm_line">2.51</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell wb60 noact"><div class="header_cell"><div class="cw"><span>ORP</span></div></div></div>'),
                    )),
                array('name' => 'salt', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell pluspx"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'salt', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb60"><div id="itm-salt-val" class="res_wrp txt_label">7.51</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell wb60 noact"><div class="header_cell"><div class="cw"><span>Salt</span></div></div></div>'),
                    )),
                array('name' => 'chlor_out', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'pool_chlor_out', 'has_sort' => 0, 'htmlRowVal' => '<div class="tbl_cell wb70"><div id="itm-pool_chlor_out-val" class="res_wrp">IC</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell wb70 noact"><div class="header_cell box_elem"><div class="cw"><span>Pool<br>Output</span></div></div></div>'),
                        array('name' => 'spa_chlor_out', 'has_sort' => 0, 'htmlRowVal' => '<div class="tbl_cell wb70"><div id="itm-spa_chlor_out-val" class="res_wrp">IC</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell wb70 noact"><div class="header_cell box_elem"><div class="cw"><span>Spa<br>Output</span></div></div></div>'),
                    )),
                array('name' => 'water_balance', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell pluspx"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'water_balance', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb70"><div id="itm-water_balance-val" class="res_wrp txt_label warning">-0.21</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell noact wb70"><div class="header_cell box_elem"><i class="icn icon5 one"></i><div class="after-img-light">Balance</div></div></div>'),
                    )),
                array('name' => 'acid', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'acid', 'has_sort' => 0, 'valPreffix' => '', 'valSuffix' => '%', 'htmlRowVal' => '<div class="tbl_cell wb50"><div class="res_wrp persent_digit success"><span  id="itm-acid-val"></span><div class="bar"></div></div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell noact wb50"><div class="header_cell"><div class="cw"><span>Acid<br>Tank</span></div></div></div>'),
                    )),
                array('name' => 'chlorine', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'chlorine', 'has_sort' => 0, 'valPreffix' => '', 'valSuffix' => '%', 'htmlRowVal' => '<div class="tbl_cell wb50"><div class="res_wrp persent_digit warning"><span id="itm-chlorine-val"></span><div class="bar"></div></div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell noact wb50"><div class="header_cell"><div class="cw"><span>Chl<br>Tank</span></div></div></div>'),
                    )),
        ));
        $allgroups['detail'] = array("groups" => array(
                array('name' => 'installation', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group  wb320"><div class="tbl_row"  style="display: block;"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell wb320"><div class="tbl_wrapper group"><div class="tbl_row  ww99" style="display: block;"></div></div></div>',
                    'cols' => array(
                        array('name' => 'status', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb40"><div class="res_wrp"><div  id="itm-status-class" class="ico_status"></div></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb40"><div class="header_cell box_elem onlystatus"><div class="cw"><div class="after-img-light">Status</div></div></div></div>'),
                        array('name' => 'name', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell wb180"><div id="itm-name-val" class="res_wrp nowrap text-left"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb180"><div class="header_cell box_elem"><div class="cw"><span>Installation Name</span></div></div></div>'),
                        array('name' => 'air_t', 'has_sort' => 0, 'valSuffix' => '°F', 'htmlRowVal' => '<div class="tbl_cell wb100"><div id="itm-air_t-val" class="res_wrp"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact wb100"><div class="header_cell box_elem icon1_bgr"><div class="cw"><span>Air<br>Temp.</span></div></div></div>'),
                        array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="installRowSubGrp" class="tbl_cell noact"></div>', 'htmlHeaderVal' => '<div id="installHeadrSubGrp" class="tbl_cell noact"></div'),
                        array('name' => 'install_adapter_serial', 'has_sort' => 0, 'valSuffix' => '', 'appendRow' => '#installRowSubGrp', 'appendHead' => '#installHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-install_adapter_serial-val" class="res_wrp nowrap isDetailMode-val inst mw150">12-34-56</div></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode inst mw150"><div class="cw top0"><span>Adapter Serial #</span></div>'),
                        array('name' => 'install_adapter_model', 'has_sort' => 0, 'valSuffix' => '', 'appendRow' => '#installRowSubGrp', 'appendHead' => '#installHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-install_adapter_model-val" class="res_wrp nowrap isDetailMode-val  inst mw150">Intelli Touch i9+3</div></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode inst mw150"><div class="cw top0"><span>Model</span></div>'),
                        array('name' => 'install_adapter_version', 'has_sort' => 0, 'valSuffix' => '', 'appendRow' => '#installRowSubGrp', 'appendHead' => '#installHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-install_adapter_version-val" class="res_wrp nowrap isDetailMode-val inst mw150">5.2.724</div></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode inst mw150"><div class="cw top0"><span>Adapter Version</span></div>'),
                        array('name' => 'install_adapter_firmware', 'has_sort' => 0, 'valSuffix' => '', 'appendRow' => '#installRowSubGrp', 'appendHead' => '#installHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-install_adapter_firmware-val" class="res_wrp nowrap isDetailMode-val inst mw150">1.160</div></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode inst mw150"><div class="cw top0"><span>Firmware</span></div>'),
                    )),
                array('name' => 'pool', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group  wb160 context_menu_edit" id="gitm-pool_point-"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell detailMode-100"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row wb160"></div></div></div>',
                    'cols' => array(
                        array('name' => 'pool_pump', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell fs-13 wb60"><div id="itm-pool_pump-val" class="res_wrp txt_status mw40"></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact detailMode-100 wb60"><div class="header_cell box_elem detailMode-100 has_sort h113"><i class="icn icn-d icon2 one"></i><div class="after-img">Pool<br>Pump</div></div></div>'),
                        array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="poolRowSubGrp" class="tbl_cell noact  wb140"></div>', 'htmlHeaderVal' => '<div id="poolHeadrSubGrp" class="tbl_cell noact wb140"></div'),
                        array('name' => 'pool_t', 'has_sort' => 0, 'valSuffix' => '°F', 'appendRow' => '#poolRowSubGrp', 'appendHead' => '#poolHeadrSubGrp', 'htmlRowVal' => '<div id="itm-pool_t-val" class="res_wrp temp mw90"></div></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem"><div class="cw top0"><span>Pool Temp</span></div>'),
                        array('name' => 'pool_point', 'has_sort' => 0, 'valSuffix' => '°F', 'appendRow' => '#poolRowSubGrp', 'appendHead' => '#poolHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-pool_point-val" class="res_wrp nowrap isDetailMode-val"></div></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode"><div class="cw top0"><span>Set Point(s)</span></div>'),
                        array('name' => 'pool_hmode', 'has_sort' => 0, 'valSuffix' => '', 'appendRow' => '#poolRowSubGrp', 'appendHead' => '#poolHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-pool_hmode-val" class="res_wrp nowrap isDetailMode-val"></div></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode"><div class="cw top0"><span>Head Mode</span></div>'),
                    )),
                array('name' => 'spa', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group  wb160 context_menu_edit" id="gitm-spa_point-"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell detailMode-100"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row wb160"></div></div></div>',
                    'cols' => array(
                        array('name' => 'spa_pump', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell fs-13 wb60"><div id="itm-spa_pump-val" class="res_wrp  mw40 txt_status off">OFF</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact detailMode-100 wb60"><div class="header_cell box_elem detailMode-100 h113"><i class="icn icn-d icon3 one"></i><div class="after-img">Spa<br>Pump</div></div></div>'),
                        array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="spaRowSubGrp" class="tbl_cell noact  wb140"></div>', 'htmlHeaderVal' => '<div id="spaHeadrSubGrp" class="tbl_cell noact wb140"></div'),
                        array('name' => 'spa_t', 'has_sort' => 0, 'valSuffix' => '°F', 'appendRow' => '#spaRowSubGrp', 'appendHead' => '#spaHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-spa_t-val" class="res_wrp mw90 temp nowrap"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem"><div class="cw top0"><span>Spa Temp</span></div></div>'),
                        array('name' => 'spa_point', 'has_sort' => 0, 'valSuffix' => '°F', 'appendRow' => '#spaRowSubGrp', 'appendHead' => '#spaHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-spa_point-val" class="res_wrp nowrap isDetailMode-val"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode"><div class="cw top0"><span>Set Point(s)</span></div></div>'),
                        array('name' => 'spa_hmode', 'has_sort' => 0, 'valSuffix' => '', 'appendRow' => '#spaRowSubGrp', 'appendHead' => '#spaHeadrSubGrp', 'htmlRowVal' => '<div  id="itm-spa_hmode-val" class="res_wrp nowrap isDetailMode-val"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode"><div class="cw top0"><span>Heat Mode</span></div></div>'),
                    )),
                array('name' => 'lights', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row wb200"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell detailMode-100"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row wb200"></div></div></div>',
                    'cols' => array(
                        array('name' => 'lights', 'has_sort' => 0, 'htmlRowVal' => '<div class="tbl_cell wb80 middle-valign"><div id="itm-lights-val" class="res_wrp from_to nowrap hasv "><span  id="itm-lights_on-val" class="from">7</span><span class="delim"> / </span><span  id="itm-lights_total-val" class="to">20</span></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact detailMode-100 wb60"><div class="header_cell box_elem detailMode-100 h113"><i class="icn icn-d icon4 one"></i><div class="after-img">Lights On / Total</div></div></div>'),
                        array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="lightsRowSubGrp" class="tbl_cell noact"></div>', 'htmlHeaderVal' => '<div id="lightsHeadrSubGrp" class="tbl_cell noact wb140"></div'),
                        array('name' => 'lights_remote', 'has_sort' => 0, 'appendRow' => '#lightsRowSubGrp', 'appendHead' => '#lightsHeadrSubGrp', 'htmlRowVal' => '<div id="itm-lights_remote-val" class="res_wrp">Enabled</div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode"><div class="cw top0"><span>Spa-Side Remote</span></div></div>'),
                        array('name' => 'lights_intelliflow', 'has_sort' => 0, 'appendRow' => '#lightsRowSubGrp', 'appendHead' => '#lightsHeadrSubGrp', 'htmlRowVal' => '<div id="itm-lights_intelliflow-val" class="res_wrp">Yes</div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode"><div class="cw top0"><span>IntelliFlow Present</span></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#lightsRowSubGrp', 'appendHead' => '#lightsHeadrSubGrp', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake"><div class="cw top0"><span>fake</span></div></div>'),
                    )),
                array('name' => 'alarms', 'htmlRowVal' => '<div class="tbl_cell flvl fs-14"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'alarms', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell fs-13 wb80"><div id="itm-alarms-val" class="res_wrp txt_label danger">3</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact center-arr wb80"><div class="header_cell detailMode-100 h113 no_icon_coll"><div class="cw after-img"><span>Alarms</span></div></div></div>'),
                        /* add fake 4str */ array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="alarmsFake" class="tbl_cell noact fake fake-1px"></div>', 'htmlHeaderVal' => '<div id="alarmsHeadFake" class="tbl_cell noact fake fake-1px"></div'),
                        array('name' => 'lights_remote', 'has_sort' => 0, 'appendRow' => '#alarmsFake', 'appendHead' => '#alarmsHeadFake', 'htmlRowVal' => '<div id="itm-lights_remote-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'lights_intelliflow', 'has_sort' => 0, 'appendRow' => '#alarmsFake', 'appendHead' => '#alarmsHeadFake', 'htmlRowVal' => '<div id="itm-lights_intelliflow-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#alarmsFake', 'appendHead' => '#alarmsHeadFake', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"><span></span></div></div>'),
                    )),
                array('name' => 'ph', 'htmlRowVal' => '<div class="tbl_cell flvl fs-14"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'ph', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell fs-13 wb50"><div id="itm-ph-val" class="res_wrp txt_label success">7.51</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact center-arr wb50"><div class="header_cell detailMode-100 h113 no_icon_coll"><div class="cw after-img"><span>pH</span></div></div></div>'),
                        /* add fake 4str */ array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="phFake" class="tbl_cell noact fake fake-1px"></div>', 'htmlHeaderVal' => '<div id="phHeadFake" class="tbl_cell noact fake fake-1px"></div'),
                        array('name' => 'lights_remote', 'has_sort' => 0, 'appendRow' => '#phFake', 'appendHead' => '#phHeadFake', 'htmlRowVal' => '<div id="itm-lights_remote-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'lights_intelliflow', 'has_sort' => 0, 'appendRow' => '#phFake', 'appendHead' => '#phHeadFake', 'htmlRowVal' => '<div id="itm-lights_intelliflow-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#phFake', 'appendHead' => '#phHeadFake', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"><span></span></div></div>'),
                    )),
                array('name' => 'orp', 'htmlRowVal' => '<div class="tbl_cell flvl fs-14"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'orp', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell fs-13 wb60"><div id="itm-orp-val" class="res_wrp txt_label warning btm_line">2.51</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact center-arr wb60"><div class="header_cell detailMode-100 h113 no_icon_coll"><div class="cw after-img"><span>ORP</span></div></div></div>'),
                        /* add fake 4str */ array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="orpFake" class="tbl_cell noact fake fake-1px"></div>', 'htmlHeaderVal' => '<div id="orpHeadFake" class="tbl_cell noact fake fake-1px"></div'),
                        array('name' => 'lights_remote', 'has_sort' => 0, 'appendRow' => '#orpFake', 'appendHead' => '#orpHeadFake', 'htmlRowVal' => '<div id="itm-lights_remote-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'lights_intelliflow', 'has_sort' => 0, 'appendRow' => '#orpFake', 'appendHead' => '#orpHeadFake', 'htmlRowVal' => '<div id="itm-lights_intelliflow-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#orpFake', 'appendHead' => '#orpHeadFake', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"><span></span></div></div>'),
                    )),
                array('name' => 'salt', 'htmlRowVal' => '<div class="tbl_cell flvl fs-14"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'salt', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell fs-13 wb60"><div id="itm-salt-val" class="res_wrp txt_label">7.51</div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell noact center-arr wb60"><div class="header_cell detailMode-100 h113 no_icon_coll"><div class="cw after-img"><span>Salt</span></div></div></div>'),
                        array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="saltFake" class="tbl_cell noact fake fake-1px"></div>', 'htmlHeaderVal' => '<div id="saltHeadFake" class="tbl_cell noact fake fake-1px"></div'),
                        array('name' => 'lights_remote', 'has_sort' => 0, 'appendRow' => '#saltFake', 'appendHead' => '#saltHeadFake', 'htmlRowVal' => '<div id="itm-lights_remote-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'lights_intelliflow', 'has_sort' => 0, 'appendRow' => '#saltFake', 'appendHead' => '#saltHeadFake', 'htmlRowVal' => '<div id="itm-lights_intelliflow-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        /* add fake 1str */ array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#saltFake', 'appendHead' => '#saltHeadFake', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"><span></span></div></div>'),
                    )),
                array('name' => 'chlor_out', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="chlorRowSubGrp" class="tbl_cell noact"></div>', 'htmlHeaderVal' => '<div id="chlorHeadrSubGrp" class="tbl_cell noact wb140 plus4px"></div'),
                        array('name' => 'pool_chlor_out', 'has_sort' => 0, 'appendRow' => '#chlorRowSubGrp', 'appendHead' => '#chlorHeadrSubGrp', 'htmlRowVal' => '<div id="itm-pool_chlor_out-val" class="res_wrp  context_menu_edit">IC</div>', 'htmlHeaderVal' => '<div class="header_cell box_elem isDetailMode"><div class="cw top0"><span>Pool Chlorine Output</span></div></div>'),
                        array('name' => 'spa_chlor_out', 'has_sort' => 0, 'appendRow' => '#chlorRowSubGrp', 'appendHead' => '#chlorHeadrSubGrp', 'htmlRowVal' => '<div id="itm-spa_chlor_out-val" class="res_wrp context_menu_edit">IC</div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode"><div class="cw top0"><span>Spa Chlorine Output</span></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#chlorRowSubGrp', 'appendHead' => '#chlorHeadrSubGrp', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake"><div class="cw top0"><span>fake</span></div></div>'),
                    )),
                array('name' => 'water_balance', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'htmlHeaderVal' => '<div class="tbl_cell"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'water_balance', 'has_sort' => 1, 'htmlRowVal' => '<div class="tbl_cell fs-14 wb70"><div id="itm-water_balance-val" class="res_wrp txt_label warning">-0.21</div></div>', 'htmlHeaderVal' => '<div class="tbl_cell noact detailMode-100 wb70"><div class="header_cell box_elem has_sort detailMode-100 h113"><i class="icn icn-d icon5 one"></i><div class="after-img">Water<br>Balance</div></div></div>'),
                        /* add fake 4str */ array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="waterFake" class="tbl_cell noact fake fake-1px"></div>', 'htmlHeaderVal' => '<div id="waterHeadFake" class="tbl_cell noact fake fake-1px"></div'),
                        array('name' => 'water_remote', 'has_sort' => 0, 'appendRow' => '#alarmsFake', 'appendHead' => '#waterHeadFake', 'htmlRowVal' => '<div id="itm-water_remote-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'water', 'has_sort' => 0, 'appendRow' => '#waterFake', 'appendHead' => '#waterHeadFake', 'htmlRowVal' => '<div id="itm-water-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#waterFake', 'appendHead' => '#waterHeadFake', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"><span></span></div></div>'),
                    )),
                array('name' => 'acid', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell mw40 m40"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'acid', 'has_sort' => 1, 'valPreffix' => '', 'valSuffix' => '%', 'htmlRowVal' => '<div class="tbl_cell vab wb50"><div class="bar persent_height success acid"><div class="colors-bg" style="height:85%"></div></div><div class="res_wrp persent_digit"><span  id="itm-acid-val"></span></div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell noact center-arr wb50"><div class="header_cell detailMode-100 h113"><div class="cw after-img d270"><span class="deg270">Acid Tank</span></div></div></div>'),
                        /* add fake 4str */ array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="acidFake" class="tbl_cell noact fake fake-1px"></div>', 'htmlHeaderVal' => '<div id="acidHeadFake" class="tbl_cell noact fake fake-1px"></div'),
                        array('name' => 'lights_remote', 'has_sort' => 0, 'appendRow' => '#acidFake', 'appendHead' => '#acidHeadFake', 'htmlRowVal' => '<div id="itm-lights_remote-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'lights_intelliflow', 'has_sort' => 0, 'appendRow' => '#acidFake', 'appendHead' => '#acidHeadFake', 'htmlRowVal' => '<div id="itm-lights_intelliflow-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#acidFake', 'appendHead' => '#acidHeadFake', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"><span></span></div></div>'),
                    )),
                array('name' => 'chlorine', 'htmlRowVal' => '<div class="tbl_cell flvl"><div class="tbl_wrapper group"><div class="tbl_row"></div></div></div>', 'htmlHeaderVal' => '<div class="tbl_cell mw40 m40"><div class="tbl_wrapper group detailMode-100"><div class="tbl_row"></div></div></div>',
                    'cols' => array(
                        array('name' => 'chlorine', 'has_sort' => 1, 'valPreffix' => '', 'valSuffix' => '%', 'htmlRowVal' => '<div class="tbl_cell vab wb50"><div class="bar persent_height warning chlorine"><div class="colors-bg" style="height:24%"></div></div><div class="res_wrp persent_digit"><span id="itm-chlorine-val"></span></div></div>',
                            'htmlHeaderVal' => '<div class="tbl_cell noact center-arr wb50"><div class="header_cell detailMode-100 h113"><div class="cw after-img d270"><span class="deg270" style="left: -2px;">Chlorine Tank</span></div></div></div>'),
                        /* add fake 4str */ array('name' => '', 'has_sort' => 0, 'htmlRowVal' => '<div id="chlorineFake" class="tbl_cell noact fake fake-1px"></div>', 'htmlHeaderVal' => '<div id="chlorineHeadFake" class="tbl_cell noact fake fake-1px"></div'),
                        array('name' => 'lights_remote', 'has_sort' => 0, 'appendRow' => '#chlorineFake', 'appendHead' => '#chlorineHeadFake', 'htmlRowVal' => '<div id="itm-lights_remote-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'lights_intelliflow', 'has_sort' => 0, 'appendRow' => '#chlorineFake', 'appendHead' => '#chlorineHeadFake', 'htmlRowVal' => '<div id="itm-lights_intelliflow-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"></div></div>'),
                        array('name' => 'fake', 'has_sort' => 0, 'appendRow' => '#chlorineFake', 'appendHead' => '#chlorineHeadFake', 'htmlRowVal' => '<div id="fake-val" class="res_wrp"></div>', 'htmlHeaderVal' => '<div class="header_cell box_elem  isDetailMode fake fake-1px"><div class="cw top0"><span></span></div></div>'),
                    )),
        ));
        foreach ($this->getVisibleGroupsList() as $value) {
            $key = array_search($value, array_column($allgroups[$this->mode]['groups'], 'name'));
            if ($key !== FALSE)
                $array['groups'][] = $allgroups[$this->mode]['groups'][$key];
        }
        return $array;
    }

    public function getThresholds() {
        $array = array(
            array('name' => 'pool_t hot', 'condition' => "row['pool_t'] > 90", 'action' => ' $("#itm-pool_t-val",fin_row).addClass( "hot" );'),
            array('name' => 'pool_t cold', 'condition' => "row['pool_t'] < 70", 'action' => ' $("#itm-pool_t-val",fin_row).addClass( "cold" );'),
            array('name' => 'pool_pump on', 'condition' => "row['pool_pump'] == 'on'", 'action' => ' $("#itm-pool_pump-val",fin_row).addClass( "on" );'),
            array('name' => 'pool_pump of', 'condition' => "row['pool_pump'] == 'off'", 'action' => ' $("#itm-pool_pump-val",fin_row).addClass( "off" );'),
            array('name' => 'pool_t last', 'condition' => "row['pool_t_last'] == 1", 'action' => ' $("#itm-pool_t-val",fin_row).append(" (last)");'),
            array('name' => 'spa_t hot', 'condition' => "row['spa_t'] > 90", 'action' => ' $("#itm-spa_t-val",fin_row).addClass( "hot" );'),
            array('name' => 'spa_t cold', 'condition' => "row['spa_t'] < 70", 'action' => ' $("#itm-spa_t-val",fin_row).addClass( "cold" );'),
            array('name' => 'spa_pump on', 'condition' => "row['spa_pump'] == 'on'", 'action' => ' $("#itm-spa_pump-val",fin_row).addClass( "on" );'),
            array('name' => 'spa_pump of', 'condition' => "row['spa_pump'] == 'off'", 'action' => ' $("#itm-spa_pump-val",fin_row).addClass( "off" );'),
            array('name' => 'spa_t last', 'condition' => "row['spa_t_last'] == 1", 'action' => ' $("#itm-spa_t-val",fin_row).append(" (last)");'),
        );
        return $array;
    }

    public function getVisibleGroupsList() {
        $this->_dbUser = new Application_Model_DbTable_User();
        $user = $this->_dbUser->getUserbyId($this->_oSession->uid);
        return empty($user->{'sortofilter_' . $this->mode}) ? $this->getAllGroupsList() : json_decode($user->{'sortofilter_' . $this->mode});
    }

    public function getAllGroupsList() {
        return ($this->mode == 'compact') ? array('installation', 'pool', 'spa', 'lights', 'alarms', 'ph', 'orp', 'salt', 'chlor_out', 'water_balance', 'acid', 'chlorine') :
                array('installation', 'pool', 'spa', 'lights', 'alarms', 'ph', 'orp', 'salt', 'chlor_out', 'water_balance', 'acid', 'chlorine');
    }

    public function saveSortofilter($groupeList) {
        $AllGroupsList = $this->getAllGroupsList();
        foreach ($groupeList as $key => $value) {
            if (!in_array($value, $AllGroupsList))
                unset($groupeList[$key]);
        }

        if (!empty($groupeList)) {
            $this->_dbUser = new Application_Model_DbTable_User();
            $user = $this->_dbUser->getUserbyId($this->_oSession->uid);
            $user->{'sortofilter_' . $this->mode} = json_encode($groupeList);
            $user->save();
        }
        return TRUE;
    }

    public function getAllPools() {
        if (!$this->pools)
            $this->pools = $this->_dbPools->getAllPool();
        return $this->pools;
    }

    public function getAllUserPools() {
        if (!$this->pools)
            $this->pools = $this->_dbPools->getPoolbyIds($this->_oSession->poolList);
        return $this->pools;
    }

    public function getAllPoolsJson() {
        $rows = $this->getAllUserPools()->toarray();
//        foreach ($rows as $k => $v) {
//            $rows[$k]["pool_t_last"] = rand(0, 1);
//            $rows[$k]["spa_t_last"] = rand(0, 1);
//        }
        return json_encode(array('mode' => $this->mode, 'header' => $this->getHeader(), 'thresholds' => $this->getThresholds(), 'rows' => $rows, 'visibleGroupsList' => $this->getVisibleGroupsList()));
    }

    public function setMode($mode) {
        $this->mode = $mode;
        return TRUE;
    }

    public function updatePools() {
        $poolList = array();
        $this->_dbUser = new Application_Model_DbTable_User();
        $users = $this->_dbUser->getActiveUsers()->toarray();
        if (!count($users)) {
            echo 'No active users';
            return;
        }
        trace($users);

        foreach ($users as $uservalue) {
            $_mPapi = new Application_Model_Papi('', '', $uservalue['sid']);
            $pools = $_mPapi->updatePoolList($uservalue['sid']);
            if (count($pools)) {
                $tmppl = array_diff($pools, $poolList);
                $poolList = array_merge($poolList, $tmppl);
                $pooldata = $_mPapi->getPoolData($uservalue['sid'],$tmppl);
                if (count($pooldata) and ! isset($pooldata['errorMessage'])) {
                    foreach ($pooldata as $value) {
                        if (!isset($value['error'])) {
                            $toSave = array(
                                'id' => (int) $value['Id'],
                                'name' => addslashes($value['poolName']),
                                'status' => $value['isStatOn'] ? "on" : "off",
                                'adp_serial' => addslashes($value['serial']),
                                'adp_ver' => addslashes($value['softVers']),
                                'firmware' => addslashes($value['firmVers']),
                                'model' => addslashes($value['model']),
                                'air_t' => (int) $value['airT'],
                                'lights_total' => (int) $value['lightsAll'],
                                'lights_on' => (int) $value['lightsOn'],
                                'spa_remote' => $value['isRemote'] ? "enabled" : "disabled",
                                'intelliflo' => $value['isIntelliFlo'] ? "yes" : "no",
                                'alarms' => (int) $value['alarmCount'],
                                'ph' => ((int) $value['pH']) / 100.0 - 0, 5,
                                'orp' => (int) $value['orp'],
                                'salt' => (int) $value['salt'],
                                'pool_chlor_out' => (int) $value['poolOut'] ? (int) $value['poolOut'] : 'IC',
                                'spa_chlor_out' => (int) $value['spaOut'] ? (int) $value['spaOut'] : 'IC',
                                'water_balance' => ((int) $value['balance']) / 100.0 - 0, 5,
                                'acid' => (int) $value['acidTank'],
                                'chlorine' => (int) $value['chlTank'],
                            );
                            $this->_dbPools->replace($toSave);
                            //trace($value);
                        } else {
                            $this->logger()->setEventItem('status', 'FAIL');
                            $this->logger()->setEventItem('trace', 'Session:' . PHP_EOL . print_r($this->_oSession, true) . PHP_EOL .
                                    "Pool data: " . PHP_EOL . print_r($value, true) . PHP_EOL);
                            $this->logger()->ERR('[API data error]: ' . $value['error']);
                        }
                    }
                }
            }
            trace("poolList");
            trace($poolList);
        }
        trace('updatePools', 1);


        return TRUE;
    }

}
