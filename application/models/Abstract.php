<?
use Govsource\Exceptions\IsEmptyException;

abstract class Application_Model_Abstract {
   

    protected $_logger;

    /**
     * @return Zend_Log
     * @throws Zend_Exception
     */
    protected function Logger()
    {
        if (!$this->_logger) {
            $this->_logger=Zend_Registry::get('Zend_Log');
        }
        return $this->_logger;
    }

}