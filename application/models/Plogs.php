<?php

class Application_Model_Plogs {

    protected $_dbPools, $_dbPlogs, $_oSession;

    public function __construct() {
        $auth = Zend_Auth::getInstance();
        $this->_oSession = $auth->getStorage()->read();
        $this->_dbPlogs = new Application_Model_DbTable_Plogs();
    }

    
    
    
    public function getLogs($filter = array(), $order = '') {
        return $this->_dbPlogs->getLogs($filter,$order);
    }
    public function addLog($pid, $itm, $val, $uid) {
        if (!$this->_dbPools) $this->_dbPools = new Application_Model_DbTable_Pools();
        $pool = $this->_dbPools->getPoolbyId($pid);
        if(empty($pool) or $pool->$itm==$val) return FALSE;
        $data= array(
            'pid'=>(int)$pid, 
            'uid'=>(int)$uid, 
            'col'=>$itm, 
            'vprev'=>$pool->$itm, 
            'vset'=>$val, 
            'error'=>''
            );
        $this->_dbPlogs->insert($data);
        return TRUE;
    }

}
