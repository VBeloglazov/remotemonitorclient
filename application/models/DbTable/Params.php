<?php
class Application_Model_DbTable_Params extends Zend_Db_Table_Abstract
{

    protected $_name   = 'slm_params';

    protected function _setupTableName() {
        parent::_setupTableName();
        $prefix = (isset($options['db']['prefix']))? $options['db']['prefix'] : '';
        $this->_name   = $prefix . $this->_name;
    }


    public function storeData ($param,$value,$description=false){
        if(!empty($param)){
            $descr = '';
            $vdescr = '';
            if(isset($param) && isset($value)){
                if(!$this->getDataByParamName($param)){
                    if(isset($description)){
                        $descr = ',description';
                        $vdescr = ",'".addslashes($description)."'";
                    }
                    $this->getAdapter()->query(
                        "REPLACE INTO ".$this->_name." (param,val".$descr.")
                        VALUES ('".addslashes($param)."','".addslashes($value)."'".$vdescr.")"
                    );
                    return $this->getAdapter()->lastInsertId($this->_name);
                } else {
                    return $this->updateData($param,$value,isset($description)?$description:false);
                }
            }
        }
        return false;
    }


    public function updateData($param,$value,$description=false){
        if(!empty($param)){
            $vdescr = '';

            if($description!==false){
                $vdescr = ",description='".addslashes($description)."'";
            }
            $this->getAdapter()->query(
                "UPDATE ".$this->_name."
                SET val='".addslashes($value)."'".$vdescr." WHERE param='".addslashes($param)."'"
            );
            return true;//$this->getAdapter()->lastInsertId($this->_name);
        }
        return false;
    }


    public function removeDataByParamName($opt){
        return $this->getAdapter()->query('DELETE FROM '.$this->_name.' WHERE param = "'.addslashes($opt).'"');
    }


    public function getDataByParamName($opt){
        return $this->fetchRow("param='".addslashes($opt)."'");
    }


}
