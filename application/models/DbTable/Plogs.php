<?php

class Application_Model_DbTable_Plogs extends Application_Model_DbTable_Abstract {

    protected $_name = 'plogs';
    // TODO: Remove after move to PHP 5.4
    //use Application_Model_Traits_Db_Table; // 5.4
    protected static $_instance;
    protected static $_fullName = '';

    protected function _setupTableName() {
        parent::_setupTableName();
        self::$_fullName = $this->_name;
    }

    public static function getTableName() {
        if (self::$_fullName != '') {
            return self::$_fullName;
        }
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем значение созданного или существующего экземпляра
        return self::$_instance->getTableName();
    }


    
    public function getLogs($filter = array(), $order = '') {
        
        $select = $this->select()->from(array('pl' => $this->getTableName()), '*');
            //->where('platform_id = ?', $platformId);
        $select->setIntegrityCheck(false);
        $select->joinLeft(
            array('u' => Application_Model_DbTable_User::getTableName()),
            'u.id = pl.uid',
            array('uname'=>'name')
        );
        $select->joinLeft(
            array('p' => Application_Model_DbTable_Pools::getTableName()),
            'p.id = pl.pid',
            array('pname'=>'name')
        );
        if (!empty($order)) $select->order($order);
        if (!empty($filter['date_from'])) {
            $date = new Zend_Date($filter['date_from']);
            $select->where('dt >= ?', $date->toString('YYYY-MM-dd HH:mm:ss'));
        }
        if (!empty($filter['date_to'])) {
            $date = new Zend_Date($filter['date_to']+86400);
            $select->where('dt < ?', $date->toString('YYYY-MM-dd HH:mm:ss'));
        }
        if (!empty($filter['column'])) {
            if($filter['column']=='pname') {
                $column = 'p.name';
        }  elseif ($filter['column']=='uname') {
                $column = 'u.name';
            } else $column = $filter['column'];
            $select->where($column.' like ?','%'.addslashes($filter['text']).'%');
        }
        return $select;

    }

}
