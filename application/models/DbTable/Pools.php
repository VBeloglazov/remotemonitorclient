<?php

class Application_Model_DbTable_Pools extends Application_Model_DbTable_Abstract {

    protected $_name = 'pools';
    // TODO: Remove after move to PHP 5.4
    //use Application_Model_Traits_Db_Table; // 5.4
    protected static $_instance;
    protected static $_fullName = '';

    protected function _setupTableName() {
        parent::_setupTableName();
        self::$_fullName = $this->_name;
    }

    public static function getTableName() {
        if (self::$_fullName != '') {
            return self::$_fullName;
        }
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем значение созданного или существующего экземпляра
        return self::$_instance->getTableName();
    }


    
    public function getPoolbyId($pid) {
        return $this->fetchRow('`id`='.(int)$pid);
    }
    public function getPoolbyIds(array $pids) {
        $select = $this->select();
        $select->where('id IN (?)', $pids);
        return $result = $this->fetchAll($select);
    }
    public function getAllPool() {
        return $this->fetchAll();
    }

}
