<?php
abstract class Application_Model_DbTable_Abstract extends Zend_Db_Table_Abstract
{

    /** @var  string Table name, should be set in table classes */
    protected $_name;
    /**
     * Should use $this->info(Zend_Db_Table_Abstract::COLS) instead
     * @deprecated */
    protected $_fields;
    protected $_logger;

    protected function _setupTableName()
    {
        parent::_setupTableName();
        $this->_name = $this->getPrefix() . $this->_name;
    }

    /**
     * Return prefix for tables
     * @return string
     */
    public function getPrefix()
    {
        return Zend_Registry::get('config')->db->prefix;
    }

    /**
     * Save data to database using id field in array
     * @param array $data
     */
    public function save(array $data)
    {
        $this->update($data, $this->_primary[1] . ' = ' . $data['id']);
    }

    public function getPrimaryColumn()
    {
        return isset($this->_primary[1]) ? $this->_primary[1] : 'id';
    }
    /**
     * @param array $data
     * @return mixed|void
     * @throws Govsource\Exceptions\DatabaseException
     */
    public function insert(array $data){
        try {
            return parent::insert($data);
        }
        catch (Exception $e) {
            $this->logger()->setEventItem('trace', print_r($data, true) . PHP_EOL . $e->getTraceAsString() . PHP_EOL);
            $this->logger()->ERR('[Database Exception]: ' . $e->getMessage());
        }
    }

    /**
     * Replace function to execute a MySQL REPLACE.
     * @param array $data data array just as if it was for insert()
     * @return Zend_Db_Statement_Mysqli
     */
    public function replace($data)
    {
        // columns submitted for insert
        $dataColumns = array_keys($data);

        // intersection of table and insert cols
        $valueColumns = array_intersect($this->info(Zend_Db_Table_Abstract::COLS), $dataColumns);
        sort($valueColumns);

        // generate SQL statement
        $cols = '';
        $vals = '';
        foreach ($valueColumns as $col) {
            $cols .= $this->getAdapter()->quoteIdentifier($col) . ',';
            $vals .= (is_object($data[$col]) && get_class($data[$col]) == 'Zend_Db_Expr')
                ? $data[$col]->__toString()
                : $this->getAdapter()->quoteInto('?', $data[$col]);
            $vals .= ',';
        }
        $cols = rtrim($cols, ',');
        $vals = rtrim($vals, ',');
        $sql = 'REPLACE INTO ' . $this->_name . ' (' . $cols . ') VALUES (' . $vals . ');';
        return $this->_db->query($sql);
    }

    public function logger()
    {
        if (!$this->_logger) $this->_logger=Zend_Registry::get('Zend_Log');
        return $this->_logger;
    }
}