<?php

class Application_Model_DbTable_User extends Application_Model_DbTable_Abstract {

    protected $_name = 'users';
    // TODO: Remove after move to PHP 5.4
    //use Application_Model_Traits_Db_Table; // 5.4
    protected static $_instance;
    protected static $_fullName = '';

    protected function _setupTableName() {
        parent::_setupTableName();
        self::$_fullName = $this->_name;
    }

    public static function getTableName() {
        if (self::$_fullName != '') {
            return self::$_fullName;
        }
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            self::$_instance = new self();
        }
        // возвращаем значение созданного или существующего экземпляра
        return self::$_instance->getTableName();
    }

    //save last request time for indicate status online/offline
    public function setOnline($id) {
        $this->update(array('last_active' => date('Y-m-d H:i:s', time())), "id=" . (int) $id);
    }
    public function setSid($uid,$sid) {
        $this->update(array('sid' => $sid), "id=" . (int) $uid);
    }

    
    public function getUserbyId($uid) {
        return $this->fetchRow('`id`='.(int)$uid);
    }
    public function getUserbyLogin($login) {
        return $this->fetchRow('`name`="'.  addslashes($login).'"');
    }
    public function getActiveUsers() {
        return $this->fetchAll('`last_active` >= DATE_SUB(NOW(),INTERVAL 15 MINUTE)');
    }

}
