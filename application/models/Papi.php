<?php

class Application_Model_Papi extends Application_Model_Abstract {

    protected $_dbPools, $_dbPlogs, $_oSession, $sessionId, $poolList, $isDebug = 0, $errorMessages = array();
    private $host = '23.253.89.11';
    private $port = '1001';
    private $httpHeader = array('X-AppVersion: TestTool', 'Content-Type: text/plain; charset=utf-8');

    public function __construct($login = "", $pass = "", $sid = 0) {
        $this->url = (strpos($this->host, 'http') === false) ? 'http://' . $this->host : $this->host;

        if (!empty($login) && !empty($pass)) {
            $this->login($login, $pass);
        } elseif ($sid) {
            $this->sessionId = $sid;
        } else {
            $this->_oSession = Zend_Auth::getInstance()->getStorage()->read();
            //if(empty($this->_oSession->sid)) throw new Zend_Controller_Action_Exception('Forbidden', 403);
            $this->sessionId = $this->_oSession->sid;
            $this->poolList = $this->_oSession->poolList;
        }
    }

    public function DebugOn() {
        $this->isDebug = 1;
    }

    public function DebugOff() {
        $this->isDebug = 0;
    }

    private function login($login, $pass) {
        $params = array(
            'loginName64' => $login, //base64_encode($login),
            'pass64' => base64_encode($pass),
        );
        $path = 'openSession';
        $response = $this->runRequest($this->getFullPath($path, $params));
        if (isset($response['sessionId']) && (int) $response['sessionId']) {
            $this->sessionId = $response['sessionId'];
            $this->poolList = $response['poolList'];
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function logout($sid = 0) {
        $params = array(
            'sessionId' => ($sid ? $sid : $this->_oSession->sid)
        );
        $path = 'closeSession';
        $response = $this->runRequest($this->getFullPath($path, $params));
        return;
    }

    public function updatePoolList($sid = 0) {
        $params = array(
            'sessionId' => ($sid ? $sid : $this->_oSession->sid)
        );
        $path = 'updatePoolList';
        $response = $this->runRequest($this->getFullPath($path, $params));
        //trace($response);
        if (isset($response['poolList'])) {
            $this->poolList = $response['poolList'];
            if (!$sid)
                $this->_oSession->poolList = $this->poolList;
        }
        return $this->poolList;
    }

    public function getPoolData($sid = 0, array $poolList = array()) {
        //$poolListSting = '"'.implode('","',!empty($poolList)?$poolList:$this->_oSession->poolList).'"';
        $maxPage = 10;
        $maxRows = 100;
        $page = 0;
        $result = array();
        $poolList = !empty($poolList) ? $poolList : $this->_oSession->poolList;
        $pollsCont = count($poolList);
        while ($page * $maxRows < $pollsCont and $page <= $maxPage) {


            if ($pollsCont <= $maxRows) {
                $poolsOnPage = $poolList;
            } else {
                $poolsOnPage = array_slice($poolList, $page * $maxRows, $maxRows);
            }

            $poolListSting = json_encode(array("poolList" => $poolsOnPage));
            $params = array(
                'sessionId' => ($sid ? $sid : $this->_oSession->sid),
                'poolList' => $poolListSting,
            );
            $path = 'getPoolData';
            $response = $this->runRequest($this->getFullPath($path), 'POST', $params);
            //trace($response);
            If (isset($response['poolData']))
                $result = array_merge($result, $response['poolData']);
            $page++;
        }



        return $result;
    }

    public function getPoolHistory($poolId, $days = 30, $sid = 0) {
        $params = array(
            'sessionId' => ($sid ? $sid : $this->_oSession->sid),
            'poolId' => $poolId,
            'historyDays' => (int) $days
        );
        $path = 'getPoolHistory';
        $response = $this->runRequest($this->getFullPath($path, $params));

        return $response;
    }

    public function getSessionId() {
        return $this->sessionId;
    }

    public function getPoolList() {
        return $this->poolList;
    }

    public function getFullPath($path, $params = array()) {
        return '/' . $path . '?' . http_build_query($params);
    }

    protected function runRequest($path, $method = 'GET', $data = '') {
        $this->responseCode = null;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->url . $path);
        curl_setopt($curl, CURLOPT_VERBOSE, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_PORT, $this->port);
        if ($this->isDebug) {
            curl_setopt($curl, CURLINFO_HEADER_OUT, TRUE);
        }
//        if (80 !== $this->port) {
//            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->checkSslCertificate);
//            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->checkSslHost);
//        }
//        $tmp = parse_url($this->url.$path);
//        $httpHeader = array();
//        if ('xml' === substr($tmp['path'], -3)) {
//            $httpHeader[] = 'Content-Type: text/xml';
//        }
//        if ('/uploads.json' === $path || '/uploads.xml' === $path) {
//            $httpHeader[] = 'Content-Type: application/octet-stream';
//        } elseif ('json' === substr($tmp['path'], -4)) {
//            $httpHeader[] = 'Content-Type: application/json';
//        }
        if (!empty($this->httpHeader)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $this->httpHeader);
        }
        if (!empty($data)) {
            $fields = '';
            foreach ($data as $key => $value) {
                $fields .= $key . '=' . $value . '&';
            }
            $fields = rtrim($fields, '&');
        }

        switch ($method) {
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, 1);
                if (isset($data)) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
                }
                break;
            case 'PUT':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                if (isset($data)) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
                }
                break;
            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default: // GET
                break;
        }
        $response = curl_exec($curl);
        $this->responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if (curl_errno($curl)) {
            $e = new \Exception(curl_error($curl), curl_errno($curl));
            curl_close($curl);
            throw $e;
        }


        if ($this->isDebug) {
            trace($this->_oSession);
            trace($path);
            trace(curl_getinfo($curl, CURLINFO_HEADER_OUT));
            trace($this->responseCode);
            trace($response);
        }

        curl_close($curl);

        if ($response) {
            // if response is XML, return an SimpleXMLElement object
            if ('<' === substr($response, 0, 1)) {
                return new SimpleXMLElement($response);
            }

            $response = json_decode($response, TRUE);
        }

        if ($this->responseCode != 200) {
            $this->errorMessages[] = isset($response['errorMessage']) ? $response['errorMessage'] : "Response Code: " . $this->responseCode;
            $this->logger()->setEventItem('status', 'FAIL');
            $this->logger()->setEventItem('trace', 'Session:' . PHP_EOL . print_r($this->_oSession, true) . PHP_EOL . "Path: " . $this->url . $path . PHP_EOL .
                    "Response Code: " . $this->responseCode . PHP_EOL . print_r($response, true) . PHP_EOL);
            $this->logger()->ERR('[API Error]: ' . (isset($response['errorMessage']) ? $response['errorMessage'] : "Response Code: " . $this->responseCode));
        }

        return $response;
    }

}
