<?php

class Inlisu_Controller_Abstract extends Zend_Controller_Action {

    /** @var  Zend_Log */
    private $_logger;

    public function preDispatch()
    {
        parent::preDispatch();
        $controller = $this->getRequest()->getControllerName();
        $action = $this->getRequest()->getActionName();
    }

    /**
     * Return the Request object
     *
     * @return Zend_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->_request;
    }

    public function isAjax()
    {
        return $this->getRequest()->getHeader('X-Requested-With') ? true : false;
    }

    public function disableLayout()
    {
        /** @var Zend_Layout $layout */
        $layout = $this->_helper->getHelper('layout');
        $layout->disableLayout();
    }
/*
EMERG   = 0;  // Emergency: system is unusable
ALERT   = 1;  // Alert: action must be taken immediately
CRIT    = 2;  // Critical: critical conditions
ERR     = 3;  // Error: error conditions
WARN    = 4;  // Warning: warning conditions
NOTICE  = 5;  // Notice: normal but significant condition
INFO    = 6;  // Informational: informational messages
DEBUG   = 7;  // Debug: debug messages
 */
    public function logger()
    {
        if (!$this->_logger) $this->_logger=Zend_Registry::get('Zend_Log');
        return $this->_logger;
    }
}