<?php
require_once 'Zend/Validate/Db/NoRecordExists.php';
    class Valid_IdentifCheck extends Zend_Validate_Db_NoRecordExists
    {
        const WE_RECOMEND_USING = 'RecordFoundAndHaveVariant';
        const INCORRECT         = 'IncorrectValue';
        const NEW_VALUE         = 'NewValue';
        protected $_messageTemplates = array(
            self::WE_RECOMEND_USING => '%value%',
            self::NEW_VALUE => '%value%'
        );
        
        public function isValid($value)
        {
            $valid = true;
            $this->_setValue($value);
            $oALM = new ALM_API();                           
                $suff = 0;
                $str = $value;

                do{
                  $_value = $value;
                  if($suff && (strlen($_value)+strlen($suff))>20){
                      $_value= substr($_value,0,strlen($_value)-strlen($suff));
                  }
                  $str =  $_value.( ($suff) ? ( $suff ) : "" );
                  $nPrj = $oALM->getProjectBYId($str);  
                  $result = $this->_query( $str );  
                  if(isset($nPrj->project->id) || $result ){
                    $suff++;
                    $valid = false;
                  }else{
                      break;
                  }
                }while(true);
                if($valid == false){
                    //$this->_createMessage(self::ERROR_RECORD_FOUND, "Identifier: \"".$value."\" - already exits. ".str_replace("[val]",$value."-".$suff,self::WE_RECOMEND_USING));
                    $this->_error(self::WE_RECOMEND_USING,"That project ID has been taken, please choose another, eg \"".$str."\".");//Identifier: \"".$value."\" - already exits. We recommend to use: \"".$str."\".");
                    $value = $str;
                    $this->_error(self::NEW_VALUE, $value);
                }
            
            return $valid;
        }
       private function _createPermalink($permalink,$id=0){
            $slnk = 'prj'.substr(preg_replace('/[^A-Za-z0-9]/', '', strtolower(trim($permalink)) ),0,255);
       }
    }