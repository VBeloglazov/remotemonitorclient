<?php
require_once 'Zend/Validate/Abstract.php';
    class Valid_GPasswordConfirmation extends Zend_Validate_Abstract
    {
        const NOT_MATCH = 'notMatch';
     
        protected $_messageTemplates = array(
            self::NOT_MATCH => 'Password does not match'
        );
     
        public function isValid($value, $context = null)
        {
            $value = (string) $value;
            $this->_setValue($value);
     
            if (is_array($context)) {
                if (isset($context['cpass'])
                    && ($value == $context['cpass']) && strlen($value)==strlen($context['cpass']))
                {
                    return true;
                }
            } elseif (is_string($context) && ($value == $context) && strlen($value)==strlen($context) ) {
                return true;
            }
     
            $this->_error(self::NOT_MATCH);
            return false;
        }
        
        //public function getMessages(){}
        //public function getErrors(){}
    }