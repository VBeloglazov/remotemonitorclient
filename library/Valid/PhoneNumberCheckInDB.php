<?php
require_once 'Zend/Validate/Abstract.php';
    class Valid_PhoneNumberCheckInDB extends Zend_Validate_Abstract
    {
        const IS_EMPTY          = 'isEmpty',
              IS_SHORT          = 'isShort',
              NO_MATCH          = 'noMatch';
     
        protected $_messageTemplates = array(
            self::IS_EMPTY          => "Value  can't be empty",
            self::IS_SHORT          => " Value is incorrect",
            self::NO_MATCH          => "Value is incorrect",
        );


        /**
         * @var string
         */
        protected $_schema = null;

        /**
         * @var string
         */
        protected $_table = '';


        /**
         * Database adapter to use. If null isValid() will use Zend_Db::getInstance instead
         *
         * @var unknown_type
         */
        protected $_adapter = null;

        /**
         * Select object to use. can be set, or will be auto-generated
         * @var Zend_Db_Select
         */
        protected $_select;



        /**
         * Returns the set adapter
         *
         * @return Zend_Db_Adapter
         */
        public function getAdapter()
        {
            /**
             * Check for an adapter being defined. if not, fetch the default adapter.
             */
            if ($this->_adapter === null) {
                $this->_adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
                if (null === $this->_adapter) {
                    require_once 'Zend/Validate/Exception.php';
                    throw new Zend_Validate_Exception('No database adapter present');
                }
            }
            return $this->_adapter;
        }







     
        public function isValid($value, $context = null)
        {
            $value = trim((string) $value);
            if(empty($value)){
                $this->_error(self::IS_EMPTY);
                return false;
            }

            $newval="";
            for($i=0;$i<strlen($value);$i++){
                if($value[$i]>="0" && $value[$i]<="9") $newval.=$value[$i];
            }
            if(strlen($newval)<6){
                $this->_error(self::IS_SHORT);
                return false;
            }

            if (is_array($context) && !empty($context['code'])){
                   $db = $this->getAdapter();
                    /**
                     * Build select object
                     */
                    $select = new Zend_Db_Select($db);
                    $front = Zend_Controller_Front::getInstance();
                    $bootstrap = $front->getParam('bootstrap');
                    $options = $bootstrap->getOptions();
                    $prefix = (isset($options['db']['prefix']))? $options['db']['prefix'] : '';
                    $select->from($prefix."users", array("id"));
                    $select->where("confirmkey = '".addslashes($context['code'])."'");
                    $select->where("phone like '%".substr($newval,strlen($newval)-6)."'");
                    //trace($select->__toString(),1);
                    $result = $db->fetchAll($select);
                    if(!sizeof($result)){
                        $this->_error(self::NO_MATCH);
                        return false;
                    }
            }else{
                $this->_error(self::NO_MATCH);
                return false;
            }

            $this->_setValue($newval);
            return true;
        }
    }