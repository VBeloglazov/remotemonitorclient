var slm = {};
slm.monitor = {
    init: function () {
        $("#sortofilter_save").on("click", slm.monitor.saveSortofilter);

        $("#groups_available, #groups_visible").sortable({
            connectWith: ".choice-line"
        }).disableSelection();
        slm.monitor.getmondata();
        slm.monitor.initChangeDialog();
        slm.monitor.initContextMenus();
        $("#sortofilter_cancel").on("click", function () {
            $('#sortofilter').hide();
            $(".noact").parent(".tbl_row").removeClass('green-border');
            //$(".noact").children(".header_cell").addClass('has_sort');
            $(".s_desc").css("opacity", 100);
            $(".s_asc").css("opacity", 100);
            $(".clh2").css("background-position", "-50px 0px");
        });
        $("#sortofilter_open").on("click", function () {
            if (!$('#table_main').length) {
                return false;
            }
            $('#sortofilter').show();
            $(".noact").parent(".tbl_row").addClass('green-border');
            //$(".noact").children(".header_cell").removeClass('has_sort');
            $(".s_desc").css("opacity", 0);
            $(".s_asc").css("opacity", 0);
            $(".clh2").css("background-position", "-50px -50px");
        });
    },
    getmondata: function (sortofilter) {
        $.post('/monitor/getdata', {'ajax': 1, 'mode': mode, 'sortofilter': sortofilter}, function (answ) {
            //console.log ( answ );
            var bgclr = "";
            var flag_color = 0;
            if (answ != 'FAIL') {
                flag_color = flag_color + 1;
                if (flag_color % 2 == 0)
                    bgclr = "cl-fffbdb";
                else
                    bgclr = "";
                $("#table_main").empty();
                $("#table_main").append('<div id="table_main_header" class="tbl_row tbl_header"></div>');

                var obj = jQuery.parseJSON(answ),
                        row_template = $('<output>').append($.parseHTML('<div class="tbl_row delim"></div><div id="table_main_tmpl_row" class="tbl_row ' + bgclr + '"></div>'));
                var groups = obj.header.groups, valFormat = {};
                for (var index in groups) {
                    if (groups.hasOwnProperty(index)) {
                        var grp = groups[index],
                                grpHtmlHead = $.parseHTML(grp.htmlHeaderVal),
                                grpHtmlRow = $.parseHTML(grp.htmlRowVal);

                        var cols = grp.cols;
                        for (var indexcols in cols) {
                            if (cols.hasOwnProperty(indexcols)) {
                                var col = cols[indexcols];
                                var tbl_cell = $.parseHTML(col.htmlHeaderVal);
                                $('.header_cell', tbl_cell).attr("itm", col['name']);
                                var appendHead = $.type(col.appendHead) === "string" ? col.appendHead : ".tbl_row",
                                        appendRow = $.type(col.appendRow) === "string" ? col.appendRow : ".tbl_row";
                                $(appendHead, grpHtmlHead).append(tbl_cell);
                                $(appendRow, grpHtmlRow).append(col.htmlRowVal);
                                if (col.has_sort) {
                                    $("[itm='" + col['name'] + "']", grpHtmlHead).addClass('has_sort').prepend('<a class="s_asc"></a><a class="s_desc"></a>');
                                }
                                valFormat[col['name']] = {'suffix': col.valSuffix, 'preffix': col.valPreffix};
                            }
                        }
                        $("#table_main_header").append(grpHtmlHead);
                        $("#table_main_tmpl_row", row_template).append(grpHtmlRow);

                    }
                }

                if ($('#sortofilter').is(':hidden')) {
                    $('#groups_visible').html('');
                    for (var vgIndex in obj.visibleGroupsList) {
                        $('#groups_visible').append('<div name="' + obj.visibleGroupsList[vgIndex] + '">' + obj.visibleGroupsList[vgIndex] + '</div>');
                    }
                    $('#groups_available').html('');
                    for (var agIndex in allGroupsList) {
                        if (jQuery.inArray(allGroupsList[agIndex], obj.visibleGroupsList) == -1)
                            $('#groups_available').append('<div name="' + allGroupsList[agIndex] + '">' + allGroupsList[agIndex] + '</div>');
                    }
                }
                $(".tbl_header .tbl_cell:first").addClass("first_coll");
                $(".tbl_header .tbl_cell:not(.group .tbl_cell):last").addClass("last_coll");
                $(".tbl_cell:first", row_template).addClass("first_coll");
                $(".tbl_cell.flvl:last", row_template).addClass("last_coll");
                //$("#table_main").append(row_template.clone().children());

                slm.monitor.populateTable(row_template, obj.rows, valFormat, obj.thresholds);
                // $(".s_asc").on("click", function(){sorting_itm = $(this).parent().attr("itm"); sorting_order = 'asc'; tblSort(sorting_itm,sorting_order);});
                // $(".s_desc").on("click", function(){sorting_itm = $(this).parent().attr("itm"); sorting_order = 'desc'; tblSort(sorting_itm,sorting_order);});
                $(".has_sort").on("click", function () {
                    //   if ($(event.target).hasClass('s_asc')||$(event.target).hasClass('s_desc')) return;
                    //console.log ( sorting_itm );  
                    if (typeof sorting_itm !== 'undefined' && sorting_itm == $(this).attr("itm")) {
                        if (sorting_order == 'asc')
                            sorting_order = 'desc';
                        else
                            sorting_order = 'asc';
                    } else
                        sorting_order = 'asc';
                    sorting_itm = $(this).attr("itm");
                    slm.monitor.tblSort(sorting_itm, sorting_order);
                });
                setTimeout(slm.monitor.getmondata, 6000000);//60000
            }

            /*    var new_width = 0;
             $('.dinamic-st').each(function () {
             var current_width = $(this).width();
             if (new_width < current_width) {new_width = current_width;}
             });
             $('.dinamic-st').width(new_width);
             $('.dinamic-st').css("min-width",new_width);
             $('.dinamic-st-header').css("min-width",new_width-30);
             $('.dinamic-st-header').css("min-width",new_width-30);
             */
        }, 'html');
    },
    populateTable: function (row_template, rows, valFormat, thresholds) {
        //console.log ( valFormat );
        for (var index in rows) {
            var fin_row = row_template.clone();
            if (rows.hasOwnProperty(index)) {
                var row = rows[index];
                for (var itm in row) {
                    var val = row[itm];
                    if (typeof valFormat[itm] !== 'undefined') {
                        if (typeof valFormat[itm]['suffix'] !== 'undefined')
                            val = val + valFormat[itm]['suffix'];
                        if (typeof valFormat[itm]['preffix'] !== 'undefined')
                            val = valFormat[itm]['preffix'] + val;
                    }
                    $("#itm-" + itm + "-val", fin_row).html(val);
                    $("#itm-" + itm + "-class", fin_row).addClass(row[itm]);

                    if (itm == 'id') {
                        $("#table_main_tmpl_row", fin_row).addClass("for_sort").attr("pid", row[itm]);
                    } else if ($("[itm='" + itm + "']").hasClass('has_sort') || jQuery.inArray(itm, ["pool_chlor_out", "spa_chlor_out", "pool_point", "spa_point", "pool_hmode", "spa_hmode"]) !== -1) {
                        $("#table_main_tmpl_row", fin_row).attr(itm, row[itm]);
                    }



                }
                for (var thIndex in thresholds) {
                    var threshold = thresholds[thIndex];
                    if (eval(threshold.condition))
                        eval(threshold.action);
                }

            }

            $("#table_main").append(fin_row.children());
        }
        if (typeof sorting_itm == 'undefined') {
            sorting_itm = 'name';
            sorting_order = 'asc';
        }
        slm.monitor.tblSort(sorting_itm, sorting_order);
    },
    tblSort: function (itm, order) {
        var $divs = $("div.for_sort");
        var OrderedDivs = $divs.sort(function (a, b) {
            if (order == 'asc') {
                if ($(a).attr(itm) < $(b).attr(itm))
                    return -1;
                if ($(a).attr(itm) > $(b).attr(itm))
                    return 1;
                return 0;
            } else {
                if ($(a).attr(itm) > $(b).attr(itm))
                    return -1;
                if ($(a).attr(itm) < $(b).attr(itm))
                    return 1;
                return 0;
            }
        });
        $("#table_main").append(OrderedDivs);
        $(".tbl_row.delim").remove();
        $("div.for_sort").before('<div class="tbl_row delim"></div>');
        $(".asc").removeClass('asc');
        $(".desc").removeClass('desc');
        $("[itm='" + itm + "']").addClass(order);

    },
    saveSortofilter: function () {
        var visibleGroup = [];
        $('#groups_visible').children().each(function (index) {
            visibleGroup.push($(this).attr('name'))
        });
        slm.monitor.getmondata(visibleGroup);
        $('#sortofilter').hide();
        $(".noact").parent(".tbl_row").removeClass('green-border');
        $(".noact").children(".header_cell").addClass('has_sort');
        $(".clh2").css("background-position", "-50px 0px");
    },
    initContextMenus: function () {
        $.contextMenu({
            selector: '.context_menu_edit',
            zIndex: 10,
            callback: function (key, options) {
                //var m = "clicked: " + key;
                //window.console && console.log(m) || alert(m); 
                //alert("Clicked on " + key + " on element " + options.$trigger.context.id);
                //console.log(options.$trigger.parents( ".for_sort" ).attr('pid'));
//                var itm = options.$trigger.context.id.split('-')[1];
                slm.monitor.prepareChangeDialog(options.$trigger);
                if (key == 'edit')
                    dialog.dialog("open");
            },
            items: {
                "edit": {name: "Change value", icon: "edit", disabled: function () {
                        var itm = $(this)[0].id.split('-')[1];
                        switch (itm) {
                            case 'pool_chlor_out':
                                if ($(this)[0].innerText == 'IC')
                                    return true;
                                break
                            case 'spa_chlor_out':
                                if ($(this)[0].innerText == 'IC')
                                    return true;
                                break
                            case 'pool_point':
//                                if ($('#itm-pool_pump-val',$(this)[0])[0].innerText.toUpperCase() == 'OFF')
//                                    return true;
                                break
                            case 'spa_point':
//                                if ($('#itm-spa_pump-val',$(this)[0])[0].innerText.toUpperCase() == 'OFF')
//                                    return true;
                                break
                        }
                        return false;
                    }}
            }
        });
    },
    prepareChangeDialog: function (trigger_obj) {
        var pid = trigger_obj.parents(".for_sort").attr('pid'),
                itm = trigger_obj.context.id.split('-')[1],
                val = trigger_obj.parents(".for_sort").attr(itm);
        var title = '';
        switch (itm) {
            case 'pool_chlor_out':
                title = 'Change Pool Chlorine output';
                $('#input_change_head_mode').hide();
                break
            case 'spa_chlor_out':
                title = 'Change Spa Chlorine output';
                $('#input_change_head_mode').hide();
                break
            case 'spa_point':
                title = 'Change Spa Set Point';
                $('#input_change_head_mode').show();
                $('input[type=radio][name=input_change_head_mode]').filter('[value="' + trigger_obj.parents(".for_sort").attr('spa_hmode') + '"]').trigger("click");
                //  alert(trigger_obj.parents(".for_sort").attr('spa_hmode'));
                break
            case 'pool_point':
                title = 'Change Pool Set Point';
                $('#input_change_head_mode').show();
                $('input[type=radio][name=input_change_head_mode]').filter('[value="' + trigger_obj.parents(".for_sort").attr('pool_hmode') + '"]').trigger("click");
                //alert(trigger_obj.parents(".for_sort").attr('pool_hmode'));
                break
            default:
                title = 'Change value';
        }
        dialog.dialog('option', 'title', title);
        $("#curValue").val(val);
        $("#input_change_value").val(val);
        $("#input_change_itm").val(itm);
        $("#input_change_pid").val(pid);
    },
    initChangeDialog: function () {

        dialog = $("#dialog-form").dialog({
            autoOpen: false,
            height: 350,
            width: 500,
            modal: true,
            buttons: {
                "Change value": slm.monitor.changeValue,
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                //form[ 0 ].reset();
                //allFields.removeClass("ui-state-error");
            }
        });
        $("#input_change_value").spinner();
        $('input[type=radio][name=input_change_head_mode]').change(function () {
            if (this.value == 'Off') {
                $("#input_change_value").spinner("disable");
            } else {
                $("#input_change_value").spinner("enable");
            }

        });
    },
    changeValue: function () {
        var data = {
            'val': $("#input_change_value").val(),
            'itm': $("#input_change_itm").val(),
            'pid': $("#input_change_pid").val()};
        if (jQuery.inArray($("#input_change_itm").val(), ["pool_point", "spa_point", ]) != -1) {
            data["hmode"] = $('input[name=input_change_head_mode]:checked').val();
        }
        $.post('/monitor/changedata', data, function (answ) {
            dialog.dialog("close");
        }, 'html');
    }

};
slm.plogs = {
    init: function () {
        $(".pagination a").on("click", slm.plogs.ChangePage);
        $(".has_sort").on("click", slm.plogs.Sort);
        $("#plog_filter_clear").on("click", slm.plogs.FilterClear);
        $("#plog_CSV_button").on("click", slm.plogs.CSVRequest);
    },
    FilterClear: function ( ) {
        $("#plog_date_from").val('');
        $("#plog_date_to").val('');
        $("#filter_column").val('');
        $("#filter_text").val('');
    },
    ChangePage: function ( ) {
        $("#plog_page").val($(this).attr("page"));
        $("#plog_filter_form").submit();
    },
    CSVRequest: function () {
        $("#plog_csv").val(1);
        $("#plog_filter_form").submit();
    },
    Sort: function ( ) {
        if ($(this).hasClass("asc")) {
            $("#plog_order").val('desc');
        } else {
            $("#plog_order").val('asc');
        }
        $("#plog_order_column").val($(this).attr("column"));
        $("#plog_filter_form").submit();
    }

};
slm.initPage = function () {
    if (typeof (current_page) == "undefined" || (typeof (slm[current_page]) == "undefined"))
        return false;
    slm[current_page].init();
    slm.initMenuButtons();
}
slm.initMenuButtons = function () {
    $("#log_page_button").on("click", function () {
        window.location.href = "/monitor/log";
    });

    $("#detail_mode").on("click", function () {
        if (!$('#table_main').length) {
            window.location.href = "/monitor/";
            return false;
        }
        if (mode == 'compact')
            mode = 'detail';
        else
            mode = 'compact';
        if (mode == 'compact')
            $(".divhide").removeClass("detaildiv");
        else
            $(".divhide").addClass("detaildiv");

        slm.monitor.getmondata();
        $("#detail_mode").toggleClass("ext");
    });
}
$(function () {
    $(document).ready(slm.initPage());

});